import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { WeatherProvider } from '../../providers/weather/weather'
import { Location } from '../../interfaces/location.interface'
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
  weather: any;
  location: Location;

  constructor(public navCtrl: NavController,
    private weatherProvider: WeatherProvider,
    private storage: Storage
    ) {

  }

  checkLocationStorageValue(value) {
    if (value !== null) {
      this.location = JSON.parse(value);

      return;
    }

    this.location = {
      city: 'Miami',
      state: 'US'
    };

    return;
  }

  ionViewWillEnter() {
    this.storage.get('location')
      .then((value) => {
        this.checkLocationStorageValue(value);

        this.weatherProvider.getWeather(this.location.city, this.location.state)
          .subscribe((weather) => {
            console.log(weather);
            this.weather = weather;
          });
      });
  }
}
