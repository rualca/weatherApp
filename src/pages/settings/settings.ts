import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Location } from '../../interfaces/location.interface';
import { Storage } from '@ionic/storage';
import { HomePage } from '../home/home';

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})

export class SettingsPage {
  location: Location;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage
  ) {
    this.location = {
      city: '',
      state: ''
    }
  }

  checkLocationStorageValue(value) {
    if (value !== null) {
      this.location = JSON.parse(value);

      return;
    }

    this.location = {
      city: 'Miami',
      state: 'US'
    };

    return;
  };

  ionViewDidLoad() {
    this.storage.get('location')
      .then((value) => {
        this.checkLocationStorageValue(value);
      });
  }

  saveForm() {
    this.storage.set('location', JSON.stringify(this.location));
    this.navCtrl.push(HomePage);
  }

}
