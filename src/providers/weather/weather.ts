import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
 
@Injectable()
export class WeatherProvider {
  apiKey = 'd8fefd09bc96fdb131f837d2cd693215';
  url: string;

  constructor(public http: Http) {
    this.url = 'http://api.openweathermap.org/data/2.5/weather?q=';
  }

  getWeather(city: string, state: string) {
    const urlComplete = `${this.url}${city}, ${state}&APPID=${this.apiKey}&units=metric`;

    return this.http.get(urlComplete)
      .map(response => response.json());
  }

}
